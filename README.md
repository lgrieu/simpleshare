# simpleshare

This is a simple wsgi python server to upload and share files in a local network.

It works with the standard python 3 library.

To run it simply write "python3 simpleshare.py", it will create two folders "shared" and "uploads" and run your default browser on port 8000.

Enjoy.
