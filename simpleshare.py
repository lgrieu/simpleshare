import sys
import os
import mimetypes
from wsgiref import simple_server, util
from string import Template
from time import strftime
import cgi
import cgitb; cgitb.enable()
import webbrowser

html = """
<!DOCTYPE html>
<html lang="fr">
<head>
<title>SimpleShare</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
    * {
  box-sizing: border-box;
}

.flex-container {
  display: flex;
  flex-direction: row;
  text-align: center;
  
}

.flex-item-left {
  background-color: #f1f1f1;
  flex: 45%;
  margin:0.5em;
  padding:1em;
  border: none;
border-radius: 5px;
}

.flex-item-right {
  background-color: #f1f1f1;
  flex: 45%;
  margin:0.5em;
  padding:1em;
  border: none;
border-radius: 5px;
}
    input[type="file"] {
        display:none;
      }
       .txt {    
      width:100%;
      display:block;
      padding:1em;
  border: none;
  border-radius: 5px;
  text-align: center;
  font-weight:bold;
  font-size:1em;
  margin: auto;
      }
    .txt:hover {
    border: 1px solid #4CAF50;
    }
      .Btn {    
      width:100%;
      display:block;
      padding:1em;
  background-color: #4CAF50;
  border: none;
  border-radius: 5px;
  color: white;
  text-align: center;
  text-decoration: none;
  font-weight:bold;
  font-size:1em;
  margin: auto;
  cursor: pointer;
  -webkit-transition-duration: 0.4s; /* Safari */
  transition-duration: 0.4s;
      }
      .Btn:hover {
  box-shadow: 0 12px 16px 0 rgba(0,0,0,0.24),0 17px 50px 0 rgba(0,0,0,0.19);
      }
      
.ValidMsg {    
      width:100%;
      display:block;
padding:1em;
border: none;
border-radius: 5px;
color: #270;
background-color: #DFF2BF;
  text-align: center;
  text-decoration: none;
  font-weight:bold;
  font-size:1em;
  margin: auto;
}
.ErrorMsg {  
      width:100%;
      display:block;
padding:1em;
border: none;
border-radius: 5px;
color: #D8000C;
background-color: #FFBABA;
  text-align: center;
  text-decoration: none;
  font-weight:bold;
  font-size:1em;
  margin: auto;
}
/* Responsive layout - makes a one column-layout instead of two-column layout */
@media (max-width: 800px) {
  .flex-container {
    flex-direction: column;
  }
}
  
    </style>
  </head>
  <body>
    
    <div class="flex-container">
      <div class="flex-item-left">
      
            <form action="/" method="POST" enctype="multipart/form-data">    
        
        <h3>Envoyer un fichier</h3>
                <input style="display: none;"  name="upload" id="upload" type="file"
                onchange="document.getElementById('FileMsg').innerText = this.files[0].name;" >
                
                <label for="upload" class="Btn" ><span id="FileMsg"  >1. Choisir un fichier</span></label>
                <br>
                <input type="text" class="txt" id="user_name" name="user_name" 
                required placeholder="2. Préciser votre nom">
                <br>
                <input type="submit" class="Btn" value="3. Envoyer le fichier" name="SubmitBtn" id="SubmitBtn" >
            </form>
            
            <br>
            
            $msg
            
            <br>
            
        </div>
        <div class="flex-item-right">
        <h3>Récuperer un fichier</h3>
        
        $shared  
        
        </div>
    </div>
  </body>
</html>

"""

succes_msg = "<div class='ValidMsg'>Fichier reçu !</div>"
        
file_error_msg = "<div class='ErrorMsg'>Erreur : Choisir un fichier !</div>"
        
name_error_msg = "<div class='ErrorMsg'>Erreur : Le nom ne peut contenir que des carracteres alphabetiques !</div>" 
     
template = Template(html) 

upload_dir = os.path.join(os.getcwd(), "uploads")
if not os.path.isdir(upload_dir):
    os.mkdir(upload_dir)
                
shared_dir = os.path.join(os.getcwd(), "shared")
if not os.path.isdir(shared_dir):
    os.mkdir(shared_dir)

def app(environ, respond):
        
    fileslist = os.listdir(shared_dir)
    
    shared_links = ''
    
    Type = 'text/html; charset=utf-8'
    
    for fileName in fileslist :
        
        shared_links += '<a href="/shared/' + fileName + '" class="Btn" target="_blank" >' + fileName + "</a><br>"
        
        if environ['PATH_INFO'] == "/shared/" + fileName:
            
            fn = os.path.join(path, environ['PATH_INFO'][1:])
            if '.' not in fn.split(os.path.sep)[-1]:
                fn = os.path.join(fn, fileName)
            Type = mimetypes.guess_type(fn)[0]

            if os.path.exists(fn):                
                respond('200 OK', [('Content-Type', Type)])
                return util.FileWrapper(open(fn, "rb"))
            
        else:
            
            Type = 'text/html; charset=utf-8'
            
    output = template.substitute({'msg' : '' , 'shared' : shared_links })
            
    if environ['REQUEST_METHOD'] == 'POST': 
        
        fields = cgi.FieldStorage(fp=environ['wsgi.input'],environ=environ, keep_blank_values=True)
        
        user_name   = fields.getvalue('user_name')
        
        upload     = fields['upload']
        
        filename = os.path.basename(upload.filename)
        name, ext = os.path.splitext(filename)
            
        if ext == '' :            
            output = template.substitute({'msg' : file_error_msg , 'shared' : shared_links  })
        
        if not user_name.isalpha():
            
            output = template.substitute({'msg' : name_error_msg , 'shared' : shared_links  })
        
        if ext != "" and user_name.isalpha() : 
                
            date = strftime("%m_%d_%Y")
            dir_path = os.path.join(upload_dir, date)     
            if not os.path.isdir(dir_path):
                os.mkdir(dir_path)
                
            time = strftime("%H-%M-%S")
            new_filename = user_name + "_" + time + ext
            file_path = os.path.join(dir_path,new_filename)
            open(file_path, 'wb').write(upload.file.read())
            
            output = template.substitute({'msg' : succes_msg , 'shared' : shared_links  })
     

    respond('200 OK', [('Content-Type', Type),('Content-Length', str(len(output)))])
    return [output.encode('utf8')]
                        
if __name__ == '__main__':
    path = sys.argv[1] if len(sys.argv) > 1 else os.getcwd()
    port = int(sys.argv[2]) if len(sys.argv) > 2 else 8000
    httpd = simple_server.make_server('', port, app)
    url = 'http://localhost:' + str(port)
    print("dossier de partage : {}, dossier de reception : {}, aperçu : {} , terminer le processus avec : control-C ".format(shared_dir,upload_dir, url))
    try:
        webbrowser.open_new(url)
        httpd.serve_forever()
    except KeyboardInterrupt:
        print("Shutting down.")
        httpd.server_close()
